# **A3_Pt1_bgea**
 
 ## **UF1. Estructures seqüencials, alternatives i iteratives.**
 <br>

## EX 1:
 ---
**Simulació d‟un compressor-descompressor**

```c
#define MAX_TEXT 100
#define MAX_VC 100
#define MAX_R 100
#define MAX_CODIS 30
#define MAX_DIGITS  5
#define MAX_COM 400
#define MAX_D 400
#define BB while(getchar()!='\n')

int main()
{

    char text[MAX_TEXT+1], vc[MAX_VC+1], caracter, auxc,codificat[MAX_COM], decodificat[MAX_D];
    int index, indexc,repeticions[MAX_R],indexr, auxr, i, maxr,u , ic, id, itc, itd;
    char codi[MAX_CODIS+1][MAX_DIGITS+1]={ "0" ,"1" ,"00" ,"01" ,"10" ,"11"
                                         ,"000" ,"001" ,"010" ,"011" ,"100"
                                         ,"101" ,"110" ,"111" ,"0000" ,"0001"
                                         , "0010" ,"0011" ,"0100" ,"0101" ,"0110"
                                         ,"0111" ,"1000" ,"1001" ,"1010" ,"1100"
                                         ,"1101" ,"1110" ,"1111" };
    //INIZIALITZACIO

    printf("Introduex un text:");
    scanf("%100[^\n]",text);BB;

    //inizialitzar el vector caractes a \0 i repeticins a 0
    for(indexc=0; indexc<MAX_VC; indexc++) vc[indexc]='\0';
    for(indexr=0; indexr<MAX_R; indexr++) repeticions[indexr]=0;
    index=0;

    while(text[index]!='\0'){
        //obtenir caracter posicio indextext de text
        caracter=text[index];
        indexc=0;
        //Buscar caracter
        while(vc[indexc]!='\0' && vc[indexc]!=caracter){
            indexc++;
        }

        if(vc[indexc]=='\0'){
           vc[indexc]=caracter;
        }else repeticions[indexc]++;

        index++;
    }

    //ordenar repeticions
    maxr=0;
    while(vc[maxr]!='\0') maxr++;
    for(i=2; i<=maxr; i++){
            for(indexc=0;indexc<=maxr-i ;indexc++){
                if(repeticions[indexc]<repeticions[indexc+1]){
                    auxr=repeticions[indexc];
                    repeticions[indexc]=repeticions[indexc+1];
                    repeticions[indexc+1]=auxr;

                    auxc=vc[indexc];
                    vc[indexc]=vc[indexc+1];
                    vc[indexc+1]=auxc;
                }
            }
        }

     u=0;
      printf("\n El vector ordenat: \n");
        while(vc[u]!='\0'){
            printf(" %c -> %i -> %s \n",vc[u],repeticions[u], codi[u]);
            u++;
         }

        index=0;
        ic=0;
        itc=0;

    //comprimir
    while(text[index]!='\0'){
        id=0;
        indexc=0;
        while(vc[indexc]!=text[index]) indexc++;
        while(codi[indexc][id]!='\0'){
            codificat[itc]=codi[indexc][id];
            id++;
            itc++;
        }
        codificat[itc]='$';
        index++;
        itc++;

    }
    codificat[itc]='\0';

    printf("\n TEXT COMPRIMIT -> %s\n",codificat);

    //Decodificar
    itc=0;
    indexc=0;
    while(codificat[itc]!='\0'){
            itd=0;
        if (codificat[itc]=='$') itc++;
        while(codificat[itc]!='\0' && codificat[itc]!='$'){
            if(codificat[itc]==vc[indexc]){
            decodificat[itd]=vc[indexc];
            itd++;
            indexc++;
            }
          decodificat[itd]='\0';
          itc++;
          itd++;
        }
    }
    decodificat[itd]='\0';

    printf("\n TEXT DECODIFICAT -> %s\n", decodificat);

    return 0;
}

```
![L’edat 100 vegades](/img/Selecci%C3%B3_005.png)
 
